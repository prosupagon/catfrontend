// Chart setup
function LoadDonutGender(element, radius, margin) {
    // Basic setup
    // ------------------------------

    // Colors
    var colors = d3.scale.category10();

    var airports = [{"key": "รวม", "values": [
        {"origin":"รวม", "carrier":"หญิง", "count":"40"},
        {"origin":"รวม", "carrier":"ชาย", "count":"70"}
    ]}];
    var total_count = 110;

    // Insert an svg element (with margin) for each row in our dataset
    var svg = d3.select(element)
        .selectAll("svg")
        .data(airports)
        .enter()
            .append("svg")
                .attr("width", (radius + margin) * 2)
                .attr("height", (radius + margin) * 2)
                .append("g")
                    .attr("transform", "translate(" + (radius + margin) + "," + (radius + margin) + ")");


    // Construct chart layout
    // ------------------------------

    // Arc
    var arc = d3.svg.arc()
        .innerRadius(radius / 2)
        .outerRadius(radius);

    // Pie
    var pie = d3.layout.pie()
        .value(function(d) { return +d.count; })
        .sort(function(a, b) { return b.count - a.count; });



    //
    // Append chart elements
    //

    // Add a label for the airport
    svg.append("text")
        .attr("dy", ".35em")
        .style("text-anchor", "middle")
        .style("font-size", 22)
        .style("font-weight", 700)
        .append('svg:tspan')
        .attr('x', 0)
        .attr('dy', -5)
        .text(function(d) { return d.key; })
        .append('svg:tspan')
        .attr('x', 0)
        .attr('dy', 30)
        .text(function(d) { return total_count; });


    // Pass the nested values to the pie layout
    var g = svg.selectAll("g")
        .data(function(d) { return pie(d.values); })
        .enter()
        .append("g")
            .attr("class", "d3-arc");


    // Add a colored arc path, with a mouseover title showing the count
    g.append("path")
        .attr("d", arc)
        .style("stroke", "#fff")
        .style("fill", function(d) { return colors(d.data.carrier); })
        .append("title")
            .text(function(d) { return d.data.carrier + ": " + d.data.count; });


    // Add a label to the larger arcs, translated to the arc centroid and rotated.
    g.filter(function(d) { return d.endAngle - d.startAngle > .2; }).append("text")
        .attr("dy", ".35em")
        .attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")rotate(" + angle(d) + ")"; })
        .style("fill", "#fff")
        .style("font-size", 14)
        .style("text-anchor", "middle")
        .append('svg:tspan')
        .attr('x', 0)
        .attr('dy', 5)
        .text(function(d) { return d.data.carrier; })
        .append('svg:tspan')
        .attr('x', 0)
        .attr('dy', 20)
        .text(function(d) { return d.data.count; });


    // Computes the label angle of an arc, converting from radians to degrees.
    function angle(d) {
        var a = (d.startAngle + d.endAngle) * 90 / Math.PI - 90;
        return a > 90 ? a - 180 : a;
    }
}

function LoadWorkCer() {
    Highcharts.setOptions({
        lang: {
            thousandsSep: ','
        }
    });

    $('#WorkCer').highcharts({
        credits : false,
        chart: {
            plotBorderWidth: 1,
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['ม.6', 'ปวช.', 'ปวส.', 'ป.ตรี', 'สูงกว่า ป.ตรี'],
            labels : {
                rotation: 0,
                style: {
                    fontSize: '10px'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            },
            stackLabels: {
                enabled: false,
                style: {
                    fontWeight: 'normal',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            enabled: false
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        fontWeight: 'normal',
                        textShadow: '0 0 3px black'
                    },
                    formatter: function(){
                        var val = this.y;
                        if (val < 0.01) {
                            return '';
                        }
                        return val;
                    }
                }
            }
        },
        series: [
            {
                type: 'column',
                name: 'Section 1',
                data: [
                    {y: 32, color: '#016BDF'}, 
                    {y: 84, color: '#FF0000'},
                    {y: 25, color: '#F57303'},
                    {y: 50, color: '#159800'},
                    {y: 14, color: '#E30196'}
                ],
                enableMouseTracking: false,
                dataLabels: {
                    enabled: true,
                    rotation: 0,
                    color: '#FFFFFF',
                    align: 'center'
                }
            }
        ]
    });
}


function LoadWorkField() {
    Highcharts.setOptions({
        lang: {
            thousandsSep: ','
        }
    });

    $('#WorkField').highcharts({
        credits : false,
        chart: {
            plotBorderWidth: 1,
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['สายงานรายได้', 'สายงานบริการ', 'สายงานการเงิน', 'สายงานบุคคล', 'สายงานปฏิบัติการ'],
            labels : {
                rotation: 0,
                style: {
                    fontSize: '10px'
                },
                formatter: function () {
                    var name = this.value['name'];
                    if (typeof name !== "undefined") {
                        name = name.replace(/ /g, '<br />');
                    }
                    return name;
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            },
            stackLabels: {
                enabled: false,
                style: {
                    fontWeight: 'normal',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            enabled: false
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        fontWeight: 'normal',
                        textShadow: '0 0 3px black'
                    },
                    formatter: function(){
                        var val = this.y;
                        if (val < 0.01) {
                            return '';
                        }
                        return val;
                    }
                }
            }
        },
        series: [
            {
                type: 'column',
                name: 'Section 1',
                data: [
                    {y: 69, color: '#016BDF'}, 
                    {y: 53, color: '#FF0000'},
                    {y: 49, color: '#F57303'},
                    {y: 55, color: '#159800'},
                    {y: 74, color: '#E30196'}
                ],
                enableMouseTracking: false,
                dataLabels: {
                    enabled: true,
                    rotation: 0,
                    color: '#FFFFFF',
                    align: 'center'
                }
            }
        ]
    });
}

function LoadWorkTime() {
    Highcharts.setOptions({
        lang: {
            thousandsSep: ','
        }
    });

    $('#WorkTime').highcharts({
        credits : false,
        chart: {
            plotBorderWidth: 1,
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['> 5 ปี', '4-5 ปี', '3-4 ปี', '2-3 ปี', '1-2 ปี', '< 1 ปี'],
            align: "right",
            labels : {
                style: {
                    fontSize: '10px'
                },
                formatter: function () {
                    var name = this.value['name'];
                    if (typeof name !== "undefined") {
                        return name;
                    }
                    return name;
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            },
            stackLabels: {
                enabled: false,
                style: {
                    fontWeight: 'normal',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            enabled: false
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        fontWeight: 'normal',
                        textShadow: '0 0 3px black'
                    },
                    formatter: function(){
                        var val = this.y;
                        if (val < 0.01) {
                            return '';
                        }
                        return val;
                    }
                }
            }
        },
        series: [
            {
                type: 'bar',
                name: 'Section 1',
                pointWidth: 20,
                data: [
                    {y: 23, color: '#016BDF'}, 
                    {y: 11, color: '#FF0000'},
                    {y: 22, color: '#F57303'},
                    {y: 41, color: '#159800'},
                    {y: 114, color: '#E30196'},
                    {y: 89, color: '#01A1E3'}
                ],
                enableMouseTracking: false,
                dataLabels: {
                    x: -5,
                    y: 1,
                    enabled: true,
                    rotation: 0,
                    color: '#FFFFFF',
                    align: 'right',
                    style: {
                        fontWeight: 'normal',
                        textShadow: '0 0 3px black'
                    }
                }
            }
        ]
    });
}


var $DashboardManpower = $('#resume');
var pieDataResume = [];
pieDataResume[0] = {
    label: "ผู้จัดการ",
    data: Math.floor(Math.random() * 100) + 1
}
pieDataResume[1] = {
    label: "ผู้จัดการทั่วไป",
    data: Math.floor(Math.random() * 100) + 1
}
pieDataResume[2] = {
    label: "หัวหน้างาน/ผู้จัดการสาขา",
    data: Math.floor(Math.random() * 100) + 1
}
pieDataResume[3] = {
    label: "พนักงาน",
    data: Math.floor(Math.random() * 100) + 1
}
pieDataResume[4] = {
    label: "เจ้าหน้าที่",
    data: Math.floor(Math.random() * 100) + 1
}

function LoadResume() {
    'use strict';
    $("#title").text("Custom Label Formatter");
    $("#description").text("Added a semi-transparent background to the labels and a custom labelFormatter function.");
    $.plot($DashboardManpower, pieDataResume, {
        series: {
            pie: {
                show: true,
                radius: 1,
                label: {
                    show: true,
                    radius: 5 / 8,
                    formatter: labelFormatterManpower,
                    background: {
                        opacity: 0
                    }
                }
            }
        },
        legend: {
            show: false
        },
        colors: ['#016BDF', '#FF0000', '#F57303', '#159800', '#E30196']
    });
}

function labelFormatterManpower(label, series) {
    'use strict';
    return "<div style='font-size:13px; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.data[0][1]) + ", " + Math.round(series.percent) + "%</div>";
}


function LoadLeave() {
    Highcharts.setOptions({
        lang: {
            thousandsSep: ','
        }
    });

    $('#Leave').highcharts({
        credits : false,
        chart: {
            plotBorderWidth: 1,
        },
        title: {
            text: ''
        },
        colors: ['#016BDF', '#FF0000', '#F57303', '#159800'],
        xAxis: {
            categories: ['สายงานรายได้', 'สายงานบริการ', 'สายงานการเงิน', 'สายงานบุคคล', 'สายงานปฏิบัติการ'],
            labels : {
                rotation: 0,
                style: {
                    fontSize: '10px'
                },
                formatter: function () {
                    var name = this.value['name'];
                    if (typeof name !== "undefined") {
                        name = name.replace(/ /g, '<br />');
                    }
                    return name;
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            },
            stackLabels: {
                enabled: false,
                style: {
                    fontWeight: 'normal',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'top',
            borderWidth: 0
        },
        tooltip: {
            // enabled: false
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        fontWeight: 'normal',
                        textShadow: '0 0 3px black'
                    },
                    formatter: function(){
                        var val = this.y;
                        if (val < 0.01) {
                            return '';
                        }
                        return val;
                    }
                }
            }
        },
        series: [
            {
                type: 'column',
                name: 'อื่นๆ',
                data: [12, 12, 9, 11, 11],
                // enableMouseTracking: false,
                dataLabels: {
                    enabled: true,
                    rotation: 0,
                    color: '#FFFFFF',
                    align: 'center'
                }
            }, 
            {
                type: 'column',
                name: 'ขาดงาน',
                data: [1, 1, 1, 0, 2],
                // enableMouseTracking: false,
                dataLabels: {
                    enabled: true,
                    rotation: 0,
                    color: '#FFFFFF',
                    align: 'center'
                }
            }, 
            {
                type: 'column',
                name: 'ลากิจ',
                data: [9, 11, 6, 9, 10],
                // enableMouseTracking: false,
                dataLabels: {
                    enabled: true,
                    rotation: 0,
                    color: '#FFFFFF',
                    align: 'center'
                }
            }, 
            {
                type: 'column',
                name: 'ลาป่วย',
                data: [44, 48, 29, 48, 26],
                // enableMouseTracking: false,
                dataLabels: {
                    enabled: true,
                    rotation: 0,
                    color: '#FFFFFF',
                    align: 'center'
                }
            }
        ]
    });
}


function LoadEmpInOut() {
    $('#EmpInOut').highcharts({
        credits : false,
        chart: {
            plotBorderWidth: 1,
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['สายงานรายได้', 'สายงานบริการ', 'สายงานการเงิน', 'สายงานบุคคล', 'สายงานปฏิบัติการ']
        },
        yAxis: {
            title: {
                text: ''
            },
            tickInterval: 1,
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            shared: true
        },
        legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'top',
            borderWidth: 0
        },
        colors: ['#159800', '#FF0000'],
        series: [{
            name: 'เริ่มงานใหม่',
            data: [4, 3, 1, 3, 5]
        }, {
            name: 'ออกจากงาน',
            data: [0, 1, 0, 1, 1]
        }]
    });
}