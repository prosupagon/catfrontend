/*------------------------------------------------------------------
 [Morrisjs  Trigger Js]

 Project     :	Fickle Responsive Admin Template
 Version     :	1.0
 Author      : 	AimMateTeam
 URL         :   http://aimmate.com
 Support     :   aimmateteam@gmail.com
 Primary use :   use on Morrisjs
 -------------------------------------------------------------------*/

jQuery(document).ready(function($) {
    'use strict';
    hero_bar_chart();
});

var resizeIdMorris;
$(window).resize(function() {
    clearTimeout(resizeIdMorris);
    resizeIdMorris= setTimeout(doneResizingMorris, 600);

});
function doneResizingMorris(){
    $('#hero-bar').html('');
    hero_bar_chart();
}

function hero_bar_chart(){
    'use strict';

    Morris.Bar({
        element: 'hero-bar',
        data: [
            {device: 'Machine 1', usage: 136},
            {device: 'Machine 2', usage: 137},
            {device: 'Machine 3', usage: 275},
            {device: 'Machine 4', usage: 380},
            {device: 'Machine 5', usage: 655},
            {device: 'Machine 6', usage: 2200},
            {device: 'Machine 7', usage: 2300},
            {device: 'Machine 8', usage: 2400},
            {device: 'Machine 9', usage: 2500},
            {device: 'Machine 10', usage: 2800},
            {device: 'Machine 11', usage: 3200},
            {device: 'Machine 12', usage: 3300},
            {device: 'Machine 13', usage: 4100},
            {device: 'Machine 14', usage: 4100},
            {device: 'Machine 15', usage: 4200},
            {device: 'Machine 16', usage: 4500},
            {device: 'Machine 17', usage: 4600},
            {device: 'Machine 18', usage: 4700},
            {device: 'Machine 19', usage: 4900},
            {device: 'Machine 20', usage: 5500}
        ],
        xkey: 'device',
        ykeys: ['usage'],
        labels: ['Usage'],
        barRatio: 0.4,
        xLabelAngle: 90,
        hideHover: 'auto',
        barColors: [$greenActive],
        resize: true
    });
}
