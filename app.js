var app = angular.module("myApp", ["ngRoute"]);

app.config(function($routeProvider) 
{	
    $routeProvider
		.when("/", {
			templateUrl : "views/portal/home.html",
			controller : "homeController"
		})
		.when("/news", {
			templateUrl : "views/portal/news.html"
		})
		.when("/about", {
			templateUrl : "views/portal/about.html"
		})
		.when("/service", {
			templateUrl : "views/portal/service.html"
		})
		.when("/contactus", {
			templateUrl : "views/portal/contactus.html"
		})
		.when("/register-1", {
			templateUrl : "views/portal/register-1.html"
		})
		.when("/register-2", {
			templateUrl : "views/portal/register-2.html",
			controller : "register2Controller"
		})
		.when("/register-3", {
			templateUrl : "views/portal/register-3.html"
		})
		.when("/register-4", {
			templateUrl : "views/portal/register-4.html",
			controller : "register4Controller"
		})
		.when("/register-5", {
			templateUrl : "views/portal/register-5.html"
		})
		.when("/dealerlogin", {
			templateUrl : "views/portal/dealerlogin.html"
		})
		.when("/catlogin", {
			templateUrl : "views/portal/catlogin.html"
		})
		.when("/firstlogin", {
			templateUrl : "views/portal/firstlogin.html"
		})
		.when("/firstlogincat", {
			templateUrl : "views/portal/firstlogincat.html"
		})
		.when("/forgotpassword", {
			templateUrl : "views/portal/forgotpassword.html"
		})
		
		
		
		.when("/dealer/CAcount", {
			templateUrl : "views/dealer/CAcount.html",
			controller : "dealerSaleAreaController"
		})
		.when("/dealer/CAList", {
			templateUrl : "views/dealer/CAList.html",
			controller : "dealerCAListController"
		})
		.when("/dealer/customer", {
			templateUrl : "views/dealer/customer.html"
		})
		.when("/dealer/dcreate-contract-account", {
			templateUrl : "views/dealer/dcreate-contract-account.html"
		})
		.when("/dealer/dealer-create-contract-acc", {
			templateUrl : "views/dealer/dealer-create-contract-acc.html"
		})
		.when("/dealer/dealer-profile-update", {
			templateUrl : "views/dealer/dealer-profile-update.html",
			controller : "dealerProfileUpdateController"
		})
		.when("/dealer/dealer-team", {
			templateUrl : "views/dealer/dealer-team.html"
		})
		.when("/dealer/dealer-user", {
			templateUrl : "views/dealer/dealer-user.html",
			controller : "dealerDealerUserController"
		})
		.when("/dealer/dregister-dealer", {
			templateUrl : "views/dealer/dregister-dealer.html"
		})
		.when("/dealer/dupdate-contract-account", {
			templateUrl : "views/dealer/dupdate-contract-account.html"
		})
		.when("/dealer/myhome", {
			templateUrl : "views/dealer/myhome.html",
			controller : "homeController"
		})
		.when("/dealer/number", {
			templateUrl : "views/dealer/number.html",
			controller : "dealerNumberController"
		})
		.when("/dealer/numbermgr", {
			templateUrl : "views/dealer/numbermgr.html",
			controller : "dealerNumbermgrController"
		})
		.when("/dealer/order", {
			templateUrl : "views/dealer/order.html",
			controller : "dealerOrderController"
		})
		.when("/dealer/registercustomer", {
			templateUrl : "views/dealer/registercustomer.html"
		})
		.when("/dealer/registercustomer-prepaid", {
			templateUrl : "views/dealer/registercustomer-prepaid.html"
		})
		.when("/dealer/req-dealer-acc-admin", {
			templateUrl : "views/dealer/req-dealer-acc-admin.html"
		})
		.when("/dealer/update-profile", {
			templateUrl : "views/dealer/update-profile.html"
		})
		.when("/dealer/updateregistercustomer", {
			templateUrl : "views/dealer/updateregistercustomer.html"
		})
		
		
		
		.when("/catuser/CATAccount", {
			templateUrl : "views/catuser/CATAccount.html"
		})
		.when("/catuser/cathome", {
			templateUrl : "views/catuser/cathome.html",
			controller : "catuserCathomeController"
		})
		.when("/catuser/cat-my-dealer", {
			templateUrl : "views/catuser/cat-my-dealer.html",
			controller : "catuserCatmydealerController"
		})
		.when("/catuser/CCAcount", {
			templateUrl : "views/catuser/CCAcount.html"
		})
		.when("/catuser/CCAList", {
			templateUrl : "views/catuser/CCAList.html",
			controller : "catuserCCAListController"
		})
		.when("/catuser/ContractCreate", {
			templateUrl : "views/catuser/ContractCreate.html",
			controller : "register4Controller"
		})
		.when("/catuser/create-cat-emp", {
			templateUrl : "views/catuser/create-cat-emp.html"
		})
		.when("/catuser/create-contract-account", {
			templateUrl : "views/catuser/create-contract-account.html"
		})
		.when("/catuser/edit-dealer-cat", {
			templateUrl : "views/catuser/edit-dealer-cat.html"
		})
		.when("/catuser/edit-package", {
			templateUrl : "views/catuser/edit-package.html"
		})
		.when("/catuser/inbox_approve_confirm", {
			templateUrl : "views/catuser/inbox_approve_confirm.html"
		})
		.when("/catuser/inbox_detail_mgr", {
			templateUrl : "views/catuser/inbox_detail_mgr.html"
		})
		.when("/catuser/inbox_detail_mkt", {
			templateUrl : "views/catuser/inbox_detail_mkt.html"
		})
		.when("/catuser/inbox_detail_sale", {
			templateUrl : "views/catuser/inbox_detail_sale.html"
		})
		.when("/catuser/jobs", {
			templateUrl : "views/catuser/jobs.html",
			controller : "catuserJobsController"
		})
		.when("/catuser/loadprepaidbulk", {
			templateUrl : "views/catuser/loadprepaidbulk.html"
		})
		.when("/catuser/mailing", {
			templateUrl : "views/catuser/mailing.html"
		})
		.when("/catuser/mdn", {
			templateUrl : "views/catuser/mdn.html"
		})
		.when("/catuser/mdnlist", {
			templateUrl : "views/catuser/mdnlist.html"
		})
		.when("/catuser/notify", {
			templateUrl : "views/catuser/notify.html",
			controller : "catuserNotifyController"
		})
		.when("/catuser/packagemgr", {
			templateUrl : "views/catuser/packagemgr.html"
		})
		.when("/catuser/provisioningSim", {
			templateUrl : "views/catuser/provisioningSim.html"
		})
		.when("/catuser/register-dealer", {
			templateUrl : "views/catuser/register-dealer.html",
			controller : "register4Controller"
		})
		.when("/catuser/RemovePre-provisioningPrepaidBulk", {
			templateUrl : "views/catuser/RemovePre-provisioningPrepaidBulk.html"
		})
		.when("/catuser/req-dealer-admin", {
			templateUrl : "views/catuser/req-dealer-admin.html"
		})
		.when("/catuser/RequestPrepaidActivationList", {
			templateUrl : "views/catuser/RequestPrepaidActivationList.html"
		})
		.when("/catuser/ReviewPrepaidActivationList", {
			templateUrl : "views/catuser/ReviewPrepaidActivationList.html"
		})
		.when("/catuser/update-contract-account", {
			templateUrl : "views/catuser/update-contract-account.html"
		})
		.when("/catuser/UpdatePrepaidDistribution", {
			templateUrl : "views/catuser/UpdatePrepaidDistribution.html"
		})
		
		.when("/catuser/catacc00_invt_mast", {
			templateUrl : "views/temp/catacc00_invt_mast.html"
		})
		.when("/catuser/catacc09_pckg_mngt", {
			templateUrl : "views/temp/catacc09_pckg_mngt.html"
		})
		.when("/catuser/ord01_pr_todolist", {
			templateUrl : "views/temp/ord01_pr_todolist.html"
		})
		.when("/catuser/ord01_request_for_order_sim", {
			templateUrl : "views/temp/ord01_request_for_order_sim.html"
		})
		
		.otherwise({ 
			redirectTo: '/' 
		});
});

app.run(function($rootScope) {

	// INIT
	$rootScope.provinces = provinces;
	$rootScope.amphures = amphures;
	$rootScope.districts = districts;
	$rootScope.titleprefix = [{ 
		value: 1,
		name: 'นาย'
	},{
		value: 2,
		name: 'นาง'
	},{
		value: 3,
		name: 'นางสาว'
	},{
		value: 4,
		name: 'พันตรี'
	},{
		value: 5,
		name: 'พันโท'
	},{
		value: 6,
		name: 'นาวาโท'
	},{
		value: 7,
		name: 'นาวาเอก'
	}];
	
	// THEME CONTROL
	$rootScope.paramPagetype = 'portal';
	
	// GLOBAL PORTAL
    $rootScope.posts = [{
		title: 'A TEXT TEXT TEXT TEXT TEXT TEXT',
		description: 'text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text',
		thumbnail: 'http://localhost/img/S_7027616113572.jpg',
		category: 1
	},{
		title: 'B TEXT TEXT TEXT TEXT TEXT TEXT',
		description: 'text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text',
		thumbnail: 'http://localhost/img/S_7027616113572.jpg',
		category: 2
	},{
		title: 'C TEXT TEXT TEXT TEXT TEXT TEXT',
		description: 'text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text',
		thumbnail: 'http://localhost/img/S_7027616113572.jpg',
		category: 1
	},{
		title: 'D TEXT TEXT TEXT TEXT TEXT TEXT',
		description: 'text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text',
		thumbnail: 'http://localhost/img/S_7027616113572.jpg',
		category: 2
	},{
		title: 'E TEXT TEXT TEXT TEXT TEXT TEXT',
		description: 'text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text text',
		thumbnail: 'http://localhost/img/S_7027616113572.jpg',
		category: 1
	}];
	
	// GLOBAL ADDRESS SELECTER
	let address = [];
	for(i in districts){
		let item = [
			provinces[districts[i].province_id].name,
			amphures[districts[i].amphur_id].name,
			districts[i].name,
			districts[i].zipcode,
			'<button type="button" class="btn btn-sm btn-primary addressselect" data-addressselect="'+districts[i].id+'">เลือก</button>'
		];
		address.push(item);
	};
	$("#mdAddress").on('show.bs.modal', function () {
		//$(this).find('input').val('');
	});
	$(document).on('click', '.addressselect', function(){
		let districtID = $(this).data('addressselect');
		$(document).find('[name="province_id"]').each(function(){
			console.log(districts[districtID]);
			//$(this).val(districts[districtID].province_id);
			$(this).val(provinces[districts[districtID].province_id].name);
		});
		$(document).find('[name="amphur_id"]').each(function(){
			//$(this).val(districts[districtID].amphur_id);
			$(this).val(amphures[districts[districtID].amphur_id].name);
		});
		$(document).find('[name="district_id"]').each(function(){
			$(this).val(districts[districtID].name);
		});
		$(document).find('[name="zipcode"]').each(function(){
			$(this).val(districts[districtID].zipcode);
		});
		$('#mdAddress').modal('hide');
	});
	$rootScope.mdAddress = $('#addressselect').DataTable({data:address, dom:'tip'});
	$('.mdAddress_search').on('keyup', function(){
		$rootScope.mdAddress
			.columns($(this).data('search'))
			.search( this.value )
			.draw();
	});
});

app.run(["$rootScope", function($rootScope) {
    $rootScope.$on("$routeChangeStart", function(event, next, current) {
        //console.log(next.$$route, next.locals); // undefined, undefined
		let params = next.$$route.originalPath;
        let paramPagetype = params.split('/')[1];
		
		switch(paramPagetype)
		{
			case 'dealer':
				$rootScope.paramPagetype = 'dealer';
				break;
			case 'catuser':
				$rootScope.paramPagetype = 'catuser';
				break;
			default:
				$rootScope.paramPagetype = 'portal';
		}
    });
}]);
	
app.controller("homeController", function ($scope, $rootScope) {	

    $scope.posts = $rootScope.posts;
	$scope.action = 'home';
	
	$scope.post = {
		title:'', description:'', thumbnail:''
	};
	
	$scope.view = function(post){
		console.log(post);
		$scope.post = post;
		$scope.action = 'view';
	}
	
	$scope.home = function(){
		$scope.action = 'home';
	}
});

app.controller("register2Controller", function ($scope, $rootScope) {		
	$scope.products = [{
		name: 'my บริการ 3G/4G',
		thumbnail: 'img/my.jpg',
		value: 1
	},{
		name: 'C Internet',
		thumbnail: 'img/c.jpg',
		value: 2
	},{
		name: 'CAT Datacom',
		thumbnail: 'img/datacom.jpg',
		value: 3
	},{
		name: 'CAT Voice',
		thumbnail: 'img/voice.jpg',
		value: 4
	},{
		name: 'CAT Data Center',
		thumbnail: 'img/datacenter.jpg',
		value: 5
	},{
		name: 'IRIS Cloud',
		thumbnail: 'img/iris.jpg',
		value: 6
	}];
});

app.controller("register4Controller", function ($scope, $rootScope) {
    
	$scope.city = $rootScope.city;
    $scope.distincts = $rootScope.distincts;
	$scope.type = 1;
	
	$rootScope.provinces = provinces;
	$rootScope.amphures = amphures;
	$rootScope.districts = districts;
	let address = [];
	for(i in districts){
		let item = [
			provinces[districts[i].province_id].name,
			amphures[districts[i].amphur_id].name,
			districts[i].name,
			districts[i].zipcode,
			'<button type="button" class="btn btn-sm btn-primary addressselect" data-addressselect="'+districts[i].id+'">เลือก</button>'
		];
		address.push(item);
	};
	
	$('#table-address').dataTable({
		data: address
	});
	$('[data-addressselect]').click(function(){
		$('[data-modaladdress="address"]').toggleClass('hidden');
		$('#previewaddress').text('123/45 หมู่.1 ถนน บรรณาการ ต.ในเมือง อ.เมือง จ.ชัยภูมิ 36000');
	});
	
	$('[name]').each(function(){
		//console.log( $(this).attr('name') );
	});
	
	$scope.changetype = function(e){
		$scope.type = e;
		console.log(e);
	}
	
});

app.controller("dealerProfileUpdateController", function ($scope, $rootScope) {	
	
    $scope.city = $rootScope.city;
    $scope.distincts = $rootScope.distincts;
});

app.controller("dealerCAListController", function ($scope, $rootScope) {	
	
    let items = [
		[1,1001113,'ประหยัด ผ่อนค่าบ้าน','CAT Internet','นนทบุรี 11','ใช้งาน'],
	];
	
	let tablecalist = $('#tablecalist').DataTable({ dom:'Btip', buttons: ['copy', 'csv', 'excel', 'pdf', 'print'] });
	setTimeout(function(){
		for(i in items){
			items[i].push('<a href="" title="" class="btn btn-sm btn-warnning fa fa-edit"></a>');
			tablecalist.row.add(items[i]).draw(false);
		}
	}, 1000);
});

app.controller("dealerSaleAreaController", function ($scope, $rootScope) {	
	
    let items = [
		[1,1001113,'A1023','ประหยัด ผ่อนค่าบ้าน','นนทบุรี 11','ใช้งาน'],
	];
	
	let tablesalelist = $('#tablesalelist').DataTable({ dom:'Btip', buttons: ['copy', 'csv', 'excel', 'pdf', 'print'] });
	setTimeout(function(){
		for(i in items){
			items[i].push('<a href="" title="" class="btn btn-sm btn-warnning fa fa-edit"></a>');
			tablesalelist.row.add(items[i]).draw(false);
		}
	}, 1000);
});

app.controller("dealerDealerUserController", function ($scope, $rootScope) {	
	
    let items = [
		['U1113','Team Mega Special Super','dealername10008','กนก จันทรา','Admin','ใช้งาน'],
	];
	
	let dealeruserlist = $('#dealeruserlist').DataTable({ dom:'Btip', buttons: ['copy', 'csv', 'excel', 'pdf', 'print'] });
	setTimeout(function(){
		for(i in items){
			items[i].push('<a href="" title="" class="btn btn-sm btn-warnning fa fa-edit"></a>');
			dealeruserlist.row.add(items[i]).draw(false);
		}
	}, 1000);
});

app.controller("dealerNumbermgrController", function ($scope, $rootScope) {	
	
    let items = [
		['U1113','AKF12193','PO1000012',4,'2017-10-23 23:59:59','ใช้งาน'],
	];
	
	let numbeermgrlist = $('#numbeermgrlist').DataTable({ dom:'Btip', buttons: ['copy', 'csv', 'excel', 'pdf', 'print'] });
	setTimeout(function(){
		for(i in items){
			items[i].push('<a href="" title="" class="btn btn-sm btn-warnning fa fa-edit"></a>');
			numbeermgrlist.row.add(items[i]).draw(false);
		}
	}, 1000);
});

app.controller("dealerNumberController", function ($scope, $rootScope) {	
	
    let items = [
		['1','0765654543','12131231231241','1231241231241','รอผล'],
	];
	
	let numberlist = $('#numberlist').DataTable({ dom:'Btip', buttons: ['copy', 'csv', 'excel', 'pdf', 'print'] });
	setTimeout(function(){
		for(i in items){
			items[i].push('<a href="" title="" class="btn btn-sm btn-warnning fa fa-edit"></a>');
			numberlist.row.add(items[i]).draw(false);
		}
	}, 1000);
});

app.controller("dealerOrderController", function ($scope, $rootScope) {	
	
    let items = [
		[201709110000771,9110000771,'สุขุม ลุ่มลึก','DL00012100','คำขออนุมัติลงทะเบียน Dealer ใหม่','Dealer Register','Loxley Co., Ltd.','27/10/2560','รอดำเนินการ'],
	];
	
	let orderlist = $('#orderlist').DataTable({ dom:'Btip', buttons: ['copy', 'csv', 'excel', 'pdf', 'print'] });
	setTimeout(function(){
		for(i in items){
			items[i].push('<a href="" title="" class="btn btn-sm btn-warnning fa fa-edit"></a>');
			orderlist.row.add(items[i]).draw(false);
		}
	}, 1000);
});

app.controller("catuserCathomeController", function ($scope, $rootScope) {	
	
    let items = [
		[1,'R0001','คำขออนุมัติลงทะเบียน Dealer ใหม่','Dealer Register','Loxley Co.,Ltd.','12/12/2060','รอดำเนินการ',''],
		[2,'R0001','คำขออนุมัติลงทะเบียน Dealer ใหม่','Dealer Register','Loxley Co.,Ltd.','12/12/2060','รอดำเนินการ',''],
		[3,'R0001','คำขออนุมัติลงทะเบียน Dealer ใหม่','Dealer Register','Loxley Co.,Ltd.','12/12/2060','รอดำเนินการ',''],
		[4,'R0001','คำขออนุมัติลงทะเบียน Dealer ใหม่','Dealer Register','Loxley Co.,Ltd.','12/12/2060','รอดำเนินการ',''],
		[5,'R0001','คำขออนุมัติลงทะเบียน Dealer ใหม่','Dealer Register','Loxley Co.,Ltd.','12/12/2060','รอดำเนินการ',''],
	];
	
	let tablelist = $('#tablelist').DataTable({ dom:'Btip', buttons: ['copy', 'csv', 'excel', 'pdf', 'print'] });
	setTimeout(function(){
		for(i in items){
			items[i].push('<a href="" title="" class="btn btn-sm btn-warnning fa fa-edit data-toggle="modal" data-target="#modal""></a>');
			tablelist.row.add(items[i]).draw(false);
		}
	}, 1000);
});

app.controller("catuserNotifyController", function ($scope, $rootScope) {	
	
    let items = [
		[1,'R0001','คำขออนุมัติลงทะเบียน Dealer ใหม่','Dealer Register','Loxley Co.,Ltd.','รอดำเนินการ',''],
	];
	
	let tablelist = $('#tablelist').DataTable({ dom:'Btip', buttons: ['copy', 'csv', 'excel', 'pdf', 'print'] });
	setTimeout(function(){
		for(i in items){
			items[i].push('<a href="" title="" class="btn btn-sm btn-warnning fa fa-edit"></a>');
			tablelist.row.add(items[i]).draw(false);
		}
	}, 1000);
});

app.controller("catuserJobsController", function ($scope, $rootScope) {	
	
    let items = [
		[1,'R0001','คำขออนุมัติลงทะเบียน Dealer ใหม่','Dealer Register','Loxley Co.,Ltd.','รอดำเนินการ','16/12/2560',''],
	];
	
	let tablelist = $('#tablelist').DataTable({ dom:'Btip', buttons: ['copy', 'csv', 'excel', 'pdf', 'print'] });
	setTimeout(function(){
		for(i in items){
			items[i].push('<a href="" title="" class="btn btn-sm btn-warnning fa fa-edit"></a>');
			tablelist.row.add(items[i]).draw(false);
		}
	}, 1000);
});

app.controller("catuserCatmydealerController", function ($scope, $rootScope) {	
	
    let items = [
		[1,'กกนก จันทรา','Dealer Register','Loxley Co.,Ltd.','รอดำเนินการ'],
	];
	
	let tablelist = $('#tablelist').DataTable({ dom:'Btip', buttons: ['copy', 'csv', 'excel', 'pdf', 'print'] });
	setTimeout(function(){
		for(i in items){
			items[i].push('<a href="" title="" class="btn btn-sm btn-warnning fa fa-edit"></a>');
			tablelist.row.add(items[i]).draw(false);
		}
	}, 1000);
});

app.controller("catuserCCAListController", function ($scope, $rootScope) {	
	
    let items = [
		[1,'CA1202','กกนก จันทรา','Dealer Register','Loxley Co.,Ltd.','รอดำเนินการ'],
	];
	
	let tablelist = $('#tablelist').DataTable({ dom:'Btip', buttons: ['copy', 'csv', 'excel', 'pdf', 'print'] });
	setTimeout(function(){
		for(i in items){
			items[i].push('<a href="" title="" class="btn btn-sm btn-warnning fa fa-edit"></a>');
			tablelist.row.add(items[i]).draw(false);
		}
	}, 1000);
});









$(document).on('click', '[data-toggle="collapse"]', function(){
	$($(this).data('href')).toggleClass('in');
});
$(document).on('click', '[data-toggle="tab"]', function(){
	$(this).parent().siblings().removeClass('active');
	$(this).parent().addClass('active');
	
	$('.tab-pane').removeClass('in active');
	$($(this).data('href')).addClass('in active');
});
$(document).on('click', '[data-toggle="modal"][data-target]', function(){
	$($(this).data('target')).modal('show');
});



















































